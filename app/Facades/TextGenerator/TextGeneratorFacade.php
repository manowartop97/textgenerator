<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/6/19
 * Time: 4:38 PM
 */

namespace App\Facades\TextGenerator;

use App\Services\TextGenerator\Contracts\TextGeneratorServiceInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Class TextGeneratorFacade
 * @package App\Facades\TextGenerator
 */
class TextGeneratorFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return TextGeneratorServiceInterface::class;
    }
}
