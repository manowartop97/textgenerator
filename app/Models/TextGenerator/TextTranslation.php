<?php

namespace App\Models\TextGenerator;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $text_id
 * @property string $locale
 * @property string $text
 * @property Text $textModel
 */
class TextTranslation extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['text'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function text()
    {
        return $this->belongsTo(Text::class);
    }
}
