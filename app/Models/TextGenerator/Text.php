<?php

namespace App\Models\TextGenerator;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $key
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property TextTranslation $textTranslation
 */
class Text extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['text'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['key', 'type', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function textTranslation()
    {
        return $this->hasOne(TextTranslation::class, 'text_id');
    }
}
