<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/6/19
 * Time: 4:03 PM
 */

namespace App\Services\TextGenerator\Contracts;

/**
 * Interface TextGeneratorServiceInterface
 * @package App\Services\TextGenerator\Contracts
 */
interface TextGeneratorServiceInterface
{
    /**
     * Text generation
     *
     * @param string|null $type
     * @param array $replacements
     * @param string|null $locale
     * @param string|null $key
     * @return string|null
     */
    public function generate(string $key = null, array $replacements = [], string $type = null, string $locale = null): ?string;
}
