<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/7/19
 * Time: 1:18 PM
 */

namespace App\Services\TextGenerator\Compilers\Contrancts;

use Exception;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Interface TextCompilerInterface
 * @package App\Services\TextGenerator\Compilers\Contrancts
 */
interface TextCompilerInterface
{
    /**
     * Return compiled string
     *
     * @param string $string
     * @param array $replacements
     * @return string
     * @throws FatalThrowableError
     * @throws Exception
     */
    public function getCompiledString(string $string, array $replacements): string;
}
