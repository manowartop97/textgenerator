<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-06
 * Time: 18:13
 */

namespace App\Services\TextGenerator\Compilers;

use App\Services\TextGenerator\Compilers\Contrancts\TextCompilerInterface;
use Exception;
use Illuminate\Support\Facades\Blade;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Throwable;

/**
 * Class CustomCompiler
 * @package App\Services\Compilers
 */
class TextCompiler implements TextCompilerInterface
{

    /**
     * Return compiled string
     *
     * @param string $string
     * @param array $replacements
     * @return string
     * @throws FatalThrowableError
     * @throws Exception
     */
    public function getCompiledString(string $string, array $replacements): string
    {
        $php = Blade::compileString($string);

        $obLevel = ob_get_level();
        ob_start();
        extract($replacements, EXTR_SKIP);

        try {
            eval('?' . '>' . $php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }

            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }

            throw new FatalThrowableError($e);
        }

        return ob_get_clean();
    }
}
