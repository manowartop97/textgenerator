<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-06
 * Time: 19:29
 */

namespace App\Services\TextGenerator\Types\Contracts;

/**
 * Interface TypeInterface
 * @package App\Services\TextGenerator\Types\Contracts
 */
interface TypeInterface
{
    /**
     * Get modified string for translation
     *
     * @param string $string
     * @return string
     */
    public function getResponse(string $string): string;
}
