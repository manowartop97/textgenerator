<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/7/19
 * Time: 11:54 AM
 */

namespace App\Services\TextGenerator\Types;

use App\Services\TextGenerator\Types\Contracts\TypeInterface;

/**
 * Class TypeDefault
 * @package App\Services\TextGenerator\Types
 */
class TypeDefault implements TypeInterface
{

    /**
     * Get modified string for translation
     *
     * @param string $string
     * @return string
     */
    public function getResponse(string $string): string
    {
        return $string;
    }
}
