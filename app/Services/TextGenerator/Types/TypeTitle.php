<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-06
 * Time: 19:30
 */

namespace App\Services\TextGenerator\Types;

use App\Services\TextGenerator\Types\Contracts\TypeInterface;

/**
 * Class TypeTitle
 * @package App\Services\TextGenerator\Types
 */
class TypeTitle implements TypeInterface
{

    /**
     * Get modified string for translation
     *
     * @param string $string
     * @return string
     */
    public function getResponse(string $string): string
    {
        return ucfirst($string);
    }
}
