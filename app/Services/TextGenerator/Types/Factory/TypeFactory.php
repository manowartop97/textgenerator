<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/7/19
 * Time: 11:52 AM
 */

namespace App\Services\TextGenerator\Types\Factory;

use App\Services\TextGenerator\Types\Contracts\TypeInterface;
use App\Services\TextGenerator\Types\TypeDefault;
use App\Services\TextGenerator\Types\TypeTitle;

/**
 * Class TypeFactory
 * @package App\Services\TextGenerator\Types\Factory
 */
class TypeFactory
{
    /**
     * Get type object by string property
     *
     * @param string|null $type
     * @return TypeInterface
     */
    public static function getTypeObject(string $type = null): TypeInterface
    {
        switch ($type) {
            case 'title':
                return resolve(TypeTitle::class);
                break;
            default:
                return resolve(TypeDefault::class);
        }
    }
}
