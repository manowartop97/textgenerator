<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 8/6/19
 * Time: 4:40 PM
 */

namespace App\Services\TextGenerator;

use App\Models\TextGenerator\Text;
use App\Services\TextGenerator\Compilers\Contrancts\TextCompilerInterface;
use App\Services\TextGenerator\Compilers\TextCompiler;
use App\Services\TextGenerator\Contracts\TextGeneratorServiceInterface;
use App\Services\TextGenerator\Types\Factory\TypeFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Class TextGeneratorService
 * @package App\Services\TextGenerator
 */
class TextGeneratorService implements TextGeneratorServiceInterface
{
    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @var TextCompiler
     */
    protected $compiler;

    /**
     * TextGeneratorService constructor.
     * @param TextCompilerInterface $compiler
     */
    public function __construct(TextCompilerInterface $compiler)
    {
        $this->defaultLocale = App::getLocale();
        $this->compiler = $compiler;
    }

    /**
     * Text generation
     *
     * @param string|null $type
     * @param array $replacements
     * @param string|null $locale
     * @param string|null $key
     * @return string|null
     * @throws FatalThrowableError
     */
    public function generate(string $key = null, array $replacements = [], string $type = null, string $locale = null): ?string
    {
        if (is_null($key) && is_null($type)) {
            return null;
        }

        $model = $this->getModelForTranslation($key, $type);
        $locale = is_null($locale) ? $this->defaultLocale : $locale;

        if (is_null($model) || !$model->hasTranslation($locale)) {
            return null;
        }

        $textToTranslate = TypeFactory::getTypeObject($type)->getResponse($model->translate($locale)->text);

        if (empty($replacements)) {
            return $textToTranslate;
        }

        return $this->getReplacedText($textToTranslate, $replacements);
    }

    /**
     * Get text for translation by method params
     *
     * @param string|null $key
     * @param string $type
     * @return Text|null
     */
    protected function getModelForTranslation(string $key = null, string $type = null): ?Model
    {
        return Text::query()
            ->when(!is_null($key), function (Builder $query) use ($key) {
                $query->where('key', $key);
            })
            ->when(!is_null($type), function (Builder $query) use ($type) {
                $query->where('type', $type);
            })
            ->when(is_null($key) || is_null($type), function (Builder $query) {
                $query->inRandomOrder();
            })
            ->first();
    }

    /**
     * Replace text with replacements
     *
     * @param string $text
     * @param array $replacements
     * @return string
     * @throws FatalThrowableError
     */
    protected function getReplacedText(string $text, array $replacements): string
    {
        return $this->compiler->getCompiledString($text, $replacements);
    }
}
