<?php

namespace App\Providers;

use App\Services\TextGenerator\Compilers\Contrancts\TextCompilerInterface;
use App\Services\TextGenerator\Compilers\TextCompiler;
use App\Services\TextGenerator\Contracts\TextGeneratorServiceInterface;
use App\Services\TextGenerator\TextGeneratorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(TextGeneratorServiceInterface::class, TextGeneratorService::class);
        $this->app->singleton(TextCompilerInterface::class, TextCompiler::class);
    }
}
