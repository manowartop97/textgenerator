<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TextTranstarions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('text_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('text_id')->unsigned();
            $table->string('locale');
            $table->text('text'); // TODO change to string if needed
            $table->unique(['text_id', 'locale']);
            $table->foreign('text_id')->on('texts')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_translations');
        Schema::dropIfExists('texts');
    }
}
