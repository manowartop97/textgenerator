<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-06
 * Time: 18:05
 */

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class TextGeneratorTest
 * @package Tests\Unit
 */
class TextGeneratorTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->seedDb();
    }

    /**
     * Translation test
     *
     * @return void
     */
    public function testTranslation(): void
    {
        $testCases = [
            [
                'key'          => 'greeting',
                'replacements' => ['name' => 'Andrew'],
                'expected'     => 'Hello Andrew',
                'locale'       => null,
                'type'         => 'title'
            ],
            [
                'key'          => 'question',
                'replacements' => ['name' => 'Andrew', 'when' => 'yesterday'],
                'expected'     => 'What did Andrew do yesterday',
                'locale'       => null,
                'type'         => null
            ],
            [
                'key'          => 'greeting',
                'replacements' => ['name' => 'Андрей'],
                'expected'     => 'Привет Андрей',
                'locale'       => 'ru',
                'type'         => null
            ],
            [
                'key'          => null,
                'replacements' => [],
                'expected'     => null,
                'locale'       => 'ru',
                'type'         => null
            ],
            [
                'key'          => null,
                'replacements' => ['name' => 'Andrew'],
                'expected'     => null,
                'locale'       => 'ru',
                'type'         => null
            ],
        ];

        foreach ($testCases as $testCase) {
            $this->assertEquals(
                $testCase['expected'],
                \TextGenerator::generate(
                    $testCase['key'],
                    $testCase['replacements'],
                    $testCase['type'],
                    $testCase['locale']
                )
            );
        }
    }

    /**
     * Seed db with start data
     *
     * @return void
     */
    protected function seedDb(): void
    {
        $data = [
            [
                'key'     => 'greeting',
                'type'    => 'title',
                'text:ru' => 'Привет {{$name}}',
                'text:en' => 'Hello {{$name}}',
            ],
            [
                'key'     => 'question',
                'type'    => 'title',
                'text:ru' => 'Что {{$name}} делал {{$when}}',
                'text:en' => 'What did {{$name}} do {{$when}}',
            ],
        ];

        foreach ($data as $dataItem) {
            \App\Models\TextGenerator\Text::query()->create($dataItem);
        }
    }
}
